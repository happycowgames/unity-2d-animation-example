﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CycleOffsetFireballScript : MonoBehaviour {
    public int TrailCount = 5;

    private int _myID;
    private static int _nextID = 0;

    private static Color[] spriteColors;

	public void Awake()
    {
        _myID = _nextID++;
        var animator = GetComponent<Animator>();
        animator.SetFloat("CycleOffset", .25f - _myID * .05f);

        if(_myID == 0)
        {
            defineSpriteColors();
            spawnOthers();
        }

        var spriteRenderer = GetComponent<SpriteRenderer>();
        spriteRenderer.sortingOrder = -_myID;
        spriteRenderer.color = spriteColors[getMyColorIndex()];
    }

    private void defineSpriteColors()
    {
        spriteColors = new Color[5];
        for(int spriteColorIndex = 0; spriteColorIndex < spriteColors.Length; spriteColorIndex++)
        {
            float colorValue = 1 - spriteColorIndex * .2f;
            spriteColors[spriteColorIndex] = new Color(colorValue, colorValue, colorValue);
        }
    }

    private void spawnOthers()
    {
        for (int i = 0; i < TrailCount; i++)
        {
            Instantiate(gameObject);
        }
    }

    private int getMyColorIndex()
    {
        if(_myID < spriteColors.Length)
        {
            return _myID;
        }

        return spriteColors.Length - 1;
    }
}
