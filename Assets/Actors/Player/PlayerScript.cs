﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerScript : MonoBehaviour {
    private Animator _animator;

	// Use this for initialization
	void Start () {
        _animator = GetComponent<Animator>();

        GameObject uiCanvas = GameObject.Find("UICanvas");
        if(uiCanvas == null)
        {
            Debug.Log("Could not find UICanvas!");
            return;
        }

        var slider = uiCanvas.transform.Find("Panel").Find("PlayerUI").Find("PlayerRunSlider").GetComponent<Slider>();
        slider.onValueChanged.AddListener(PlayerRunSliderValueChanged);

        var isGroundedToggle = uiCanvas.transform.Find("Panel").Find("PlayerUI").Find("IsGroundedToggle").GetComponent<Toggle>();
        isGroundedToggle.onValueChanged.AddListener(IsGroundedToggleChanged);
	}

    void PlayerRunSliderValueChanged(float val)
    {
        if(val < 1)
        {
            _animator.SetBool("IsRunning", false);
        }
        else if (val < 2)
        {
            _animator.SetBool("IsRunning", true);
            _animator.SetFloat("RunningSpeed", 1);
        }
        else
        {
            _animator.SetBool("IsRunning", true);
            _animator.SetFloat("RunningSpeed", 4);
        }
    }

    void IsGroundedToggleChanged(bool val)
    {
        _animator.SetBool("IsGrounded", val);
    }
}
