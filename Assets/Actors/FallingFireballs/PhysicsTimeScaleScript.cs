﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PhysicsTimeScaleScript : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        GetComponent<Slider>().onValueChanged.AddListener(TimeScaleSliderValueChanged);
    }

    void TimeScaleSliderValueChanged(float val)
    {
        Time.timeScale = val;
    }
}
