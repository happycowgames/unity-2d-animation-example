﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FireballNormalizedScript : MonoBehaviour {
    Animator _animator;

    // Use this for initialization
    void Start()
    {
        _animator = GetComponent<Animator>();

        var uiPanelTransform = GameObject.Find("UICanvas").transform.Find("Panel");

        var slider = uiPanelTransform.Find("fireballNormalizedTimeSlider").GetComponent<Slider>();
        slider.onValueChanged.AddListener(sliderValueChanged);
    }

    public void sliderValueChanged(float val)
    {
        _animator.SetFloat("normalizedTime", val);
    }
}
