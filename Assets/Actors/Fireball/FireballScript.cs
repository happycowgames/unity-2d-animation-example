﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FireballScript : MonoBehaviour {

    Animator _animator;
    private bool _acceptTrigger;

	// Use this for initialization
	void Start () {
        _animator = GetComponent<Animator>();

        var uiPanelTransform = GameObject.Find("UICanvas").transform.Find("Panel");

        var button = uiPanelTransform.Find("fireballJumpButton").GetComponent<Button>();
        button.onClick.AddListener(JumpButtonClicked);

        _acceptTrigger = true;
    }

    public void JumpButtonClicked()
    {
        if (!_acceptTrigger)
        {
            return;
        }

        _acceptTrigger = false;
        _animator.SetTrigger("TriggerJump");
    }

    public void JumpComplete()
    {
        _acceptTrigger = true;
    }
}
